import keras
from typing import *
import numpy as np
import os.path

from datetime import datetime


class Fcn:
    model: keras.Sequential

    fit_params = dict(
        batch_size=32,
        epochs=20,
        verbose=1
    )

    def __init__(self, input_shape: Tuple[int, int, int], fit_params=None, weights: str = None):
        if fit_params is not None:
            self.fit_params = fit_params
        self.model = keras.Sequential()

        self.model.add(keras.layers.Layer(input_shape=input_shape))
        self.model.add(keras.layers.Conv2D(filters=32, kernel_size=10, padding='same', strides=1,
                                           activation=keras.activations.relu))
        self.model.add(keras.layers.BatchNormalization())
        self.model.add(
            keras.layers.Conv2D(filters=16, kernel_size=4, padding='same', strides=1, activation=keras.activations.relu))
        self.model.add(keras.layers.BatchNormalization())
        self.model.add(
            keras.layers.Conv2D(filters=8, kernel_size=4, padding='same', strides=1, activation=keras.activations.relu))
        self.model.add(keras.layers.BatchNormalization())
        self.model.add(keras.layers.Conv2D(filters=47, kernel_size=1))
        self.model.add(keras.layers.BatchNormalization())
        self.model.add(keras.layers.Activation('softmax'))

        if weights is not None:
            self.model.load_weights(weights)

        self.model.compile(keras.optimizers.adam(), loss=keras.losses.categorical_crossentropy, metrics=['accuracy'])
        print(self.model.summary())

    def fit(self, x, y, val_data: Tuple[np.ndarray, np.ndarray]):
        date = datetime.now().strftime("%m-%d-%Y_%H-%M-%S")
        tb_call_back = keras.callbacks.TensorBoard(log_dir=f"./Graph/{date}/", histogram_freq=1, write_graph=True,
                                                   write_images=True)
        checkpoint = keras.callbacks.ModelCheckpoint('./Model/'+date+'/weights-epoch-{epoch:02d}-val_acc-{val_acc:.2f}.hdf5',
                                                     monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        os.mkdir(f"./Model/{date}")
        self.model.fit(x, y, validation_data=val_data, callbacks=[tb_call_back, checkpoint], **self.fit_params)

    def validate(self, x, y):
        return self.model.evaluate(x, y)
