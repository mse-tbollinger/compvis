import glob

import numpy as np
import skimage
import skimage.io
import skimage.io

from typing import *

import keras.preprocessing


class Preprocessor:
    x_train: np.ndarray
    x_test: np.ndarray
    x_val: np.ndarray

    y_train: np.ndarray
    y_test: np.ndarray
    y_val: np.ndarray

    image_shape: Tuple[int, int]

    def __init__(self, x_train, x_test, x_val, y_train, y_test, y_val):
        self.x_train = x_train
        self.x_test = x_test
        self.x_val = x_val
        self.y_train = keras.preprocessing.utils.to_categorical(y_train, num_classes=47)
        self.y_test = keras.preprocessing.utils.to_categorical(y_test, num_classes=47)
        self.y_val = keras.preprocessing.utils.to_categorical(y_val, num_classes=47)
        self.image_shape = self.x_train.shape[1:3]

    @staticmethod
    def from_directories(test_path: str, train_path: str, val_path: str):
        x_test_raw_images = Preprocessor._load_x_images(test_path)
        x_train_raw_images = Preprocessor._load_x_images(train_path)
        x_val_raw_images = Preprocessor._load_x_images(val_path)

        y_test_raw_images = Preprocessor._load_y_images(test_path)
        y_train_raw_images = Preprocessor._load_y_images(train_path)
        y_val_raw_images = Preprocessor._load_y_images(val_path)

        image_shape = x_train_raw_images.shape[1:3]

        new_shape = (-1, image_shape[0], image_shape[1], 1)
        x_train = x_test_raw_images.reshape(new_shape)
        x_test = x_train_raw_images.reshape(new_shape)
        x_val = x_val_raw_images.reshape(new_shape)

        y_train = y_test_raw_images
        y_test = y_train_raw_images
        y_val = y_val_raw_images

        return Preprocessor(x_train, x_test, x_val, y_train, y_test, y_val)

    def __str__(self):
        return \
            f"x_train:\t {self.x_train.shape}\n" + \
            f"x_test :\t {self.x_test.shape}\n" + \
            f"x_val  :\t {self.x_val.shape}\n" + \
            f"y_train:\t {self.y_train.shape}\n" + \
            f"y_test :\t {self.y_test.shape}\n" + \
            f"y_val  :\t {self.y_val.shape}\n"

    @staticmethod
    def _load_images(path: str, filter: str):
        # path = path + '/'
        files = list(glob.glob(path + filter, recursive=False))

        first = skimage.io.imread(files[0])
        (h, w) = first.shape

        images = np.zeros((len(files), h, w), dtype=np.int8)

        for i, file in enumerate(files):
            images[i, :, :] = skimage.io.imread(files[i])
        return images

    def save_data(self, path: str):
        np.save(path + "/x_train", self.x_train)
        np.save(path + "/x_test", self.x_test)
        np.save(path + "/x_val", self.x_val)

        np.save(path + "/y_train", np.argmax(self.y_train, axis=3))
        np.save(path + "/y_test", np.argmax(self.y_test, axis=3))
        np.save(path + "/y_val", np.argmax(self.y_val, axis=3))

    @staticmethod
    def load(path: str):
        return Preprocessor(np.load(path + "/x_train.npy"),
                            np.load(path + "/x_test.npy"),
                            np.load(path + "/x_val.npy"),
                            np.load(path + "/y_train.npy"),
                            np.load(path + "/y_test.npy"),
                            np.load(path + "/y_val.npy"),
                            )

    @staticmethod
    def _load_x_images(path: str):
        return Preprocessor._load_images(path, "/image*.png")

    @staticmethod
    def _load_y_images(path: str):
        return Preprocessor._load_images(path, "/label*.png")


