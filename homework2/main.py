from fcn import Fcn
from cnn import Cnn
from preprocessor import Preprocessor
import os.path

if __name__ == "__main__":
    p: Preprocessor

    if os.path.isfile("data/x_train.npy"):
        p = Preprocessor.load("data")
    else:
        p = Preprocessor.from_directories('data/dtd_test', 'data/dtd_train', 'data/dtd_val')
        p.save_data("data")
    print(p)

    # fcn = Fcn((p.image_shape[0], p.image_shape[1], 1))
    # fcn.fit(p.x_train, p.y_train, (p.x_test, p.y_test))
    # print(fcn.validate(p.x_val, p.y_val))

    cnn = Cnn((p.image_shape[0], p.image_shape[1], 1),
              weights="Model2/05-17-2019_18-26-52/weights-epoch-40-val_acc-0.09.hdf5")
    cnn.fit(p.x_train, p.y_train, (p.x_test, p.y_test))
    print(cnn.validate(p.x_val, p.y_val))

