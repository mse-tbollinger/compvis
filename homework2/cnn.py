import keras
from typing import *
import numpy as np
import os.path

from datetime import datetime


class Cnn:
    model: keras.Sequential

    fit_params = dict(
        batch_size=32,
        epochs=20,
        verbose=2
    )

    cnn_settings = [
        dict(
            filters=16,
            kernel_size=5,
            strides=1
        ),
        dict(
            filters=16,
            kernel_size=3,
            strides=2
        ),
        dict(
            filters=32,
            kernel_size=3,
            strides=2
        ),
        dict(
            filters=64,
            kernel_size=2,
            strides=1
        ),
    ]

    pool_settings = [
        dict(
            pool_size=2
        ),
        dict(
            pool_size=0
        ),
        dict(
            pool_size=2
        ),
        dict(
            pool_size=2
        )
    ]

    def __init__(self, input_shape: Tuple[int, int, int], fit_params=None, weights: str = None):
        if fit_params is not None:
            self.fit_params = fit_params
        self.model = keras.Sequential()

        self.model.add(keras.layers.Layer(input_shape=input_shape))

        n = len(self.cnn_settings)
        for i in range(n):
            self.model.add(keras.layers.Conv2D(padding='same', activation=keras.activations.relu, **self.cnn_settings[i]))
            self.model.add(keras.layers.BatchNormalization())
            if self.pool_settings[i]['pool_size'] != 0:
                self.model.add(keras.layers.MaxPooling2D(**self.pool_settings[i]))

        for i in reversed(range(len(self.cnn_settings))):
            if self.pool_settings[i]['pool_size'] != 0:
                self.model.add(keras.layers.UpSampling2D(size=self.pool_settings[i]['pool_size']))
            self.model.add(
                keras.layers.Conv2DTranspose(padding='same', activation=keras.activations.relu, **self.cnn_settings[i]))

        self.model.add(keras.layers.Conv2D(filters=47, kernel_size=1))
        self.model.add(keras.layers.Activation('softmax'))

        if weights is not None:
            self.model.load_weights(weights)

        self.model.compile(keras.optimizers.adam(), loss=keras.losses.categorical_crossentropy, metrics=['accuracy'])
        print(self.model.summary())

    def fit(self, x, y, val_data: Tuple[np.ndarray, np.ndarray]):
        date = datetime.now().strftime("%m-%d-%Y_%H-%M-%S")
        dir = f"./Model2/{date}"
        tb_call_back = keras.callbacks.TensorBoard(log_dir=f"./Graph2/{date}/", histogram_freq=0,
                                                   write_graph=True,
                                                   write_images=False)
        checkpoint = keras.callbacks.ModelCheckpoint(
            './Model2/' + date + '/weights-epoch-{epoch:02d}-val_acc-{val_acc:.2f}.hdf5',
            monitor='val_acc', verbose=1, save_best_only=True, mode='max', period=5)
        os.mkdir(f"./Model2/{date}")
        self.log_to_file(dir)
        self.model.fit(x, y, validation_data=val_data, callbacks=[tb_call_back, checkpoint], **self.fit_params)

    def log_to_file(self, path: str):
        with open(f"{path}/settings.txt", "w") as fh:
            fh.write(f"{self.cnn_settings} {self.pool_settings} \n")
            self.model.summary(print_fn=lambda x: fh.write(x + '\n'))

    def validate(self, x, y):
        return self.model.evaluate(x, y)
